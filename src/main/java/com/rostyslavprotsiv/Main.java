package com.rostyslavprotsiv;

import com.rostyslavprotsiv.view.Menu;

public class Main {
    public static void main(final String[] args) {
        Menu menu = Menu.getInstance();
        menu.show();
    }
}

package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.ArrayAction;
import com.rostyslavprotsiv.model.action.HeroAction;
import com.rostyslavprotsiv.model.creator.ArrayCreator;
import com.rostyslavprotsiv.model.creator.RoomCreator;
import com.rostyslavprotsiv.model.entity.Room;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Controller {
    private static final Controller INSTANCE = new Controller();
    private static final RoomCreator ROOM_CREATOR = new RoomCreator();
    private static final Room ROOM = ROOM_CREATOR.create();
    private static final HeroAction HERO_ACTION = new HeroAction();
    private static final ArrayAction ARRAY_ACTION = new ArrayAction();
    private static final ArrayCreator ARRAY_CREATOR = new ArrayCreator();
    private static final int[] FIRST_ARR = ARRAY_CREATOR.create();
    private static final int[] SECOND_ARR = ARRAY_CREATOR.create();
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    private Controller() {
    } // private constructor

    public static Controller getInstance() {
        return INSTANCE;
    }

    public int getFatalDoors() {
        return HERO_ACTION.findAllMortalDoors(ROOM);
    }

    public String[] getCreatedRoom() {
        String[] doorInfo = new String[Room.MAX_DOORS];
        for (int i = 0; i < Room.MAX_DOORS; i++) {
            doorInfo[i] = ROOM.getDoorBehinders()[i].toString();
        }
        if (doorInfo.length == 0) {
            String logMessage = "Used incorrect length : " + doorInfo.length;
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        return doorInfo;
    }

    public String getOrderOfDoors() {
        return ARRAY_ACTION.getStringMass(
                HERO_ACTION.getNeededForAliveDoors(ROOM));
    }

    public String getAllArrays() {
        StringBuilder allArrays = new StringBuilder();
        allArrays.append(ARRAY_ACTION.getStringMass(FIRST_ARR) + "\n");
        allArrays.append(ARRAY_ACTION.getStringMass(SECOND_ARR));
        return allArrays.toString();
    }

    public String getGeneralElements() {
        return ARRAY_ACTION.getStringMass(ARRAY_ACTION.
                getGeneralElements(FIRST_ARR, SECOND_ARR));
    }

    public String getSeparateElements() {
        return ARRAY_ACTION.getStringMass(ARRAY_ACTION.getSeparateElements(
                FIRST_ARR, SECOND_ARR, true));
    }

    public String deleteRepeatedTwoMore() {
        return ARRAY_ACTION.getStringMass(
                ARRAY_ACTION.deleteRepeatedTwoMore(FIRST_ARR));
    }

    public String deleteSimilarExceptOne() {
        return ARRAY_ACTION.getStringMass(
                ARRAY_ACTION.deleteSimilarExceptOne(FIRST_ARR));
    }

    public int getMaxDoors() {
        return Room.MAX_DOORS;
    }
}

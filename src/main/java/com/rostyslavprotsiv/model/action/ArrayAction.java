package com.rostyslavprotsiv.model.action;

public class ArrayAction {

    public final int[] getGeneralElements(
            final int[] first, final int[] second) {
        int[] copyOfFirst = copy(first);
        int[] copyOfSecond = copy(second);
        int[] finalArr = new int[0];
        for (int i = 0; i < copyOfFirst.length; i++) {
            for (int j = 0; j < copyOfSecond.length; j++) {
                if (copyOfFirst[i] == copyOfSecond[j]) {
                    finalArr = add(finalArr, copyOfFirst[i]);
                    copyOfFirst = remove(copyOfFirst, i);
                    i--;
                    copyOfSecond = remove(copyOfSecond, j);
                    break;
                }
            }
        }
        return finalArr;
    }

    public final int[] getSeparateElements(final int[] first,
                                           final int[] second,
                                           final boolean needFirstMass) {
        int[] copyOfFirst = copy(first);
        int[] copyOfSecond = copy(second);
        for (int i = 0; i < copyOfFirst.length; i++) {
            for (int j = 0; j < copyOfSecond.length; j++) {
                if (copyOfFirst[i] == copyOfSecond[j]) {
                    copyOfFirst = remove(copyOfFirst, i);
                    i--;
                    copyOfSecond = remove(copyOfSecond, j);
                    break;
                }
            }
        }
        if (needFirstMass) {
            return copyOfFirst;
        } else {
            return copyOfSecond;
        }
    }

    public final int[] deleteRepeatedTwoMore(final int[] arr) {
        int[] copyOfArr = copy(arr);
        int counter = 0;
        for (int i = 0; i < copyOfArr.length; i++) {
            for (int j = i + 1; j < copyOfArr.length; j++) {
                if (copyOfArr[i] == copyOfArr[j]) {
                    counter++;
                    if (counter > 1) {
                        copyOfArr = remove(copyOfArr, j);
                        j--;
                    }
                }
            }
            counter = 0;
        }
        return copyOfArr;
    }

    public final int[] deleteSimilarExceptOne(final int[] arr) {
        int[] copyOfArr = copy(arr);
        for (int i = 0; i < copyOfArr.length - 1; i++) {
            if (copyOfArr[i] == copyOfArr[i + 1]) {
                copyOfArr = remove(copyOfArr, i + 1);
                i--;
            }
        }
        return copyOfArr;
    }

    public final String getStringMass(final int[] mass) {
        StringBuilder result = new StringBuilder();
        if (mass.length > 0) {
            for (int elem : mass) {
                result.append(elem);
                result.append(", ");
            }
            result.deleteCharAt(result.lastIndexOf(","));
        }
        return result.toString();
    }

    private int[] copy(final int[] mass) {
        int[] copy = new int[mass.length];
        for (int i = 0; i < mass.length; i++) {
            copy[i] = mass[i];
        }
        return copy;
    }

    private int[] add(final int[] mass, final int element) {
        int[] expanded = new int[mass.length + 1];
        for (int i = 0; i < mass.length; i++) {
            expanded[i] = mass[i];
        }
        expanded[mass.length] = element;
        return expanded;
    }

    private int[] remove(final int[] mass, final int index) {
        int[] changed = new int[mass.length - 1];
        for (int i = 0, j = 0; i < mass.length
                && j < mass.length - 1; i++, j++) {
            if (index == i) {
                i++;
                changed[j] = mass[i];
                continue;
            }
            changed[j] = mass[i];
        }
        return changed;
    }
}

package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Hero;
import com.rostyslavprotsiv.model.entity.Room;
import com.rostyslavprotsiv.model.entity.doorentities.DoorBehinder;

public class HeroAction {
    private static final Hero HERO = new Hero(
            "Strikoza"); //is good to be used later
    private static final RoomAction ROOM_ACTION = new RoomAction();
    private int counterOfDoors;
    private int numOfDoors;

    public final int findAllMortalDoors(final Room room) {
        DoorBehinder[] creations = room.getDoorBehinders();
        if (creations[numOfDoors].heroInteraction() + Hero.POWER <= 0) {
            counterOfDoors++;
        }
        numOfDoors++;
        if (numOfDoors < Room.MAX_DOORS) {
            return findAllMortalDoors(room);
        } else {
            return counterOfDoors;
        }
    }

    public final int[] getNeededForAliveDoors(final Room room) {
        int[] order = new int[Room.MAX_DOORS];
        if (canBeAlive(room)) {
            int[][] doors = ROOM_ACTION.sortPoints(room);
            for (int i = 0; i < Room.MAX_DOORS; i++) {
                order[i] = doors[i][0] + 1;
            }
        }
        System.out.println(canBeAlive(room));
        return order;
    }


    private boolean canBeAlive(final Room room) {
        int allPoints = Hero.POWER;
        DoorBehinder[] creations = room.getDoorBehinders();
        for (DoorBehinder creation : creations) {
            allPoints += creation.heroInteraction();
        }
        if (allPoints > 0) {
            return true;
        }
        return false;
    }
}

package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Room;
import com.rostyslavprotsiv.model.entity.doorentities.DoorBehinder;

public class RoomAction {
    public final int[][] sortPoints(final Room room) {
        int[][] doorAndPoints = new int[Room.MAX_DOORS][2];
        fillDoorAndPoints(doorAndPoints, room);
        sortDoorAndPoints(doorAndPoints);
        return doorAndPoints;
    }

    private void fillDoorAndPoints(final int[][] doorAndPoints,
                                   final Room room) {
        DoorBehinder[] creations = room.getDoorBehinders();
        for (int k = 0; k < Room.MAX_DOORS; k++) {
            doorAndPoints[k][0] = k;
            doorAndPoints[k][1] = creations[k].heroInteraction();
        }
    }

    private void sortDoorAndPoints(final int[][] doorAndPoints) {
        for (int k = 1; k < doorAndPoints.length; k++) {
            for (int i = k; i > 0
                    && doorAndPoints[i][1] > doorAndPoints[i - 1][1]; i--) {
                swap(doorAndPoints, i, i - 1);
            }
        }
    }

    private void swap(final int[][] doorAndPoints,
                      final int firstElem, final int secondElem) {
        int helper, helper1;
        helper = doorAndPoints[firstElem][0];
        helper1 = doorAndPoints[firstElem][1];
        doorAndPoints[firstElem][0] = doorAndPoints[secondElem][0];
        doorAndPoints[firstElem][1] = doorAndPoints[secondElem][1];
        doorAndPoints[secondElem][0] = helper;
        doorAndPoints[secondElem][1] = helper1;
    }
}

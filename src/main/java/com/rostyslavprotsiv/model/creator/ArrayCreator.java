package com.rostyslavprotsiv.model.creator;

import java.util.Random;

public class ArrayCreator {
    private static final Random RANDOM = new Random();

    public final int[] create() {
        final int maxNumberOfElements = 15;
        final int possibleElements = 10;
        final int otherElements = 5;
        int size = RANDOM.nextInt(maxNumberOfElements) + 1;
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = RANDOM.nextInt(possibleElements) - otherElements;
        }
        return arr;
    }
}

package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.entity.Room;
import com.rostyslavprotsiv.model.entity.doorentities.Artifact;
import com.rostyslavprotsiv.model.entity.doorentities.DoorBehinder;
import com.rostyslavprotsiv.model.entity.doorentities.Monster;
import com.rostyslavprotsiv.model.exceptions.ArtifactLogicalException;
import com.rostyslavprotsiv.model.exceptions.MonsterLogicalException;
import com.rostyslavprotsiv.model.exceptions.RoomLogicalException;

import java.util.Random;

public class RoomCreator {
    private static final Random RANDOM = new Random();

    public final Room create() {
        DoorBehinder[] all = new DoorBehinder[Room.MAX_DOORS];
        DoorBehinder entity;
        final int minForArtifact = 10;
        final int maxForArtifact = 80;
        final int minForMonster = 5;
        final int maxForMonster = 100;
        int pointsForArtifact;
        int pointsForMonster;
        try {
            for (int i = 0; i < Room.MAX_DOORS; i++) {
                if (RANDOM.nextBoolean()) {
                    pointsForArtifact = RANDOM.nextInt(maxForArtifact
                            - minForArtifact) + minForArtifact + 1;
                    entity = new Artifact(pointsForArtifact);
                } else {
                    pointsForMonster = RANDOM.nextInt(maxForMonster
                            - minForMonster) + minForMonster + 1;
                    entity = new Monster(pointsForMonster);
                }
                all[i] = entity;
            }
            return new Room(all);
        } catch (RoomLogicalException | ArtifactLogicalException
                | MonsterLogicalException e) {
            e.printStackTrace();
        }
        return new Room();
    }
}

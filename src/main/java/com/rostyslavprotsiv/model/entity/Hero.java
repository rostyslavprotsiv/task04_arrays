package com.rostyslavprotsiv.model.entity;

import java.util.Objects;

public class Hero {
    public static final int POWER = 25;
    private String name;

    public Hero(final String name) {
        this.name = name;
    }

    public Hero() {
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Hero hero = (Hero) o;
        return Objects.equals(name, hero.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Hero{"
                + "name='" + name + '\''
                + '}';
    }
}

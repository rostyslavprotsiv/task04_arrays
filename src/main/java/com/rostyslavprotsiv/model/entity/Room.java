package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.doorentities.DoorBehinder;
import com.rostyslavprotsiv.model.exceptions.RoomLogicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * This class is a room with 10 doors.
 *
 * @author Rostyslav Protsiv
 * @version 1.0.0 16 April 2019
 * @since 2019-04-16
 */
public class Room {
    public static final int MAX_DOORS = 10;
    private DoorBehinder[] doorBehinders;
    private static final Logger LOGGER = LogManager.getLogger(Room.class);

    public Room() {
    }

    public Room(final DoorBehinder[] doorBehinders)
            throws RoomLogicalException {
        if (checkSize(doorBehinders)) {
            this.doorBehinders = doorBehinders;
        } else {
            logMessages();
            throw new RoomLogicalException();
        }
    }

    public DoorBehinder[] getDoorBehinders() {
        return doorBehinders;
    }

    public void setDoorBehinders(final DoorBehinder[] doorBehinders)
            throws RoomLogicalException {
        if (checkSize(doorBehinders)) {
            this.doorBehinders = doorBehinders;
        } else {
            logMessages();
            throw new RoomLogicalException();
        }
    }

    private boolean checkSize(final DoorBehinder[] doorBehinders) {
        return doorBehinders.length == MAX_DOORS;
    }

    private void logMessages() {
        String logMessage = "Entered incorrect length : "
                + doorBehinders.length;
        LOGGER.trace(logMessage);
        LOGGER.debug(logMessage);
        LOGGER.info(logMessage);
        LOGGER.warn(logMessage);
        LOGGER.error(logMessage);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Room room = (Room) o;
        return Arrays.equals(doorBehinders, room.doorBehinders);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(doorBehinders);
    }

    @Override
    public String toString() {
        return "Room{"
                + "doorBehinders=" + Arrays.toString(doorBehinders)
                + '}';
    }
}

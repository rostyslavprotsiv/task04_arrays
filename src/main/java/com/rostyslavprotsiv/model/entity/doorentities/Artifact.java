package com.rostyslavprotsiv.model.entity.doorentities;

import com.rostyslavprotsiv.model.exceptions.ArtifactLogicalException;

public class Artifact extends DoorBehinder {

    public Artifact() {
    }

    public Artifact(final int points) throws ArtifactLogicalException {
        if (checkPoints(points)) {
            this.points = points;
        } else {
            throw new ArtifactLogicalException();
        }
    }


    public void setPoins(final int points) throws ArtifactLogicalException {
        if (checkPoints(points)) {
            this.points = points;
        } else {
            throw new ArtifactLogicalException();
        }
    }

    @Override
    public int heroInteraction() {
        return points;
    }

    private boolean checkPoints(final int points) {
        final int minPoints = 10;
        final int maxPoints = 80;
        return points >= minPoints && points <= maxPoints;
    }

    @Override
    public String toString() {
        return "Artifact{"
                + "points=" + points
                + '}';
    }
}


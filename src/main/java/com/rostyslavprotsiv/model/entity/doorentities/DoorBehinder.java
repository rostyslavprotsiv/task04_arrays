package com.rostyslavprotsiv.model.entity.doorentities;

import java.util.Objects;

public abstract class DoorBehinder {
    protected int points;

    public abstract int heroInteraction();

    public int getPoints() {
        return points;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DoorBehinder that = (DoorBehinder) o;
        return points == that.points;
    }

    @Override
    public int hashCode() {
        return Objects.hash(points);
    }

    @Override
    public String toString() {
        return "DoorBehinder{"
                + "points=" + points
                + '}';
    }
}

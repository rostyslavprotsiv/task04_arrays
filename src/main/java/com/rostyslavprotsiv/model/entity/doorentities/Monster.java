package com.rostyslavprotsiv.model.entity.doorentities;

import com.rostyslavprotsiv.model.exceptions.MonsterLogicalException;

public class Monster extends DoorBehinder {

    public Monster() {
    }

    public Monster(final int points) throws MonsterLogicalException {
        if (checkPoints(points)) {
            this.points = points;
        } else {
            throw new MonsterLogicalException();
        }
    }


    public void setPoins(final int points)
            throws MonsterLogicalException {
        if (checkPoints(points)) {
            this.points = points;
        } else {
            throw new MonsterLogicalException();
        }
    }

    @Override
    public int heroInteraction() {
        return -points;
    }

    private boolean checkPoints(final int points) {
        final int minPoints = 5;
        final int maxPoints = 100;
        return points >= minPoints && points <= maxPoints;
    }

    @Override
    public String toString() {
        return "Monster{"
                + "points=" + points
                + '}';
    }
}

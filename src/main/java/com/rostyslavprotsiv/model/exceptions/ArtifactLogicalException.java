package com.rostyslavprotsiv.model.exceptions;

public class ArtifactLogicalException extends Exception {
    public ArtifactLogicalException() {
    }

    public ArtifactLogicalException(final String s) {
        super(s);
    }

    public ArtifactLogicalException(final String s,
                                    final Throwable throwable) {
        super(s, throwable);
    }

    public ArtifactLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public ArtifactLogicalException(final String s,
                                    final Throwable throwable,
                                    final boolean b, final boolean b1) {
        super(s, throwable, b, b1);
    }
}

package com.rostyslavprotsiv.model.exceptions;

public class MonsterLogicalException extends Exception {
    public MonsterLogicalException() {
    }

    public MonsterLogicalException(final String s) {
        super(s);
    }

    public MonsterLogicalException(final String s,
                                   final Throwable throwable) {
        super(s, throwable);
    }

    public MonsterLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public MonsterLogicalException(final String s, final Throwable throwable,
                                   final boolean b, final boolean b1) {
        super(s, throwable, b, b1);
    }
}

package com.rostyslavprotsiv.model.exceptions;

public class RoomLogicalException extends Exception {
    public RoomLogicalException() {
    }

    public RoomLogicalException(final String s) {
        super(s);
    }

    public RoomLogicalException(final String s,
                                final Throwable throwable) {
        super(s, throwable);
    }

    public RoomLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public RoomLogicalException(final String s, final Throwable throwable,
                                final boolean b, final boolean b1) {
        super(s, throwable, b, b1);
    }
}

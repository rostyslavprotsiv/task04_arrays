package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Menu {
    private static final Menu INSTANCE = new Menu();
    private static final Controller CONTROLLER = Controller.getInstance();
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);


    private Menu() {
    } // private constructor

    public static Menu getInstance() {
        return INSTANCE;
    }

    public void show() {
        showArrOperations();
        showGame();
    }

    private void showGame() {
        final int sizeForExpandingCreations = 10;
        final int sizeForExpandingPoints = 5;
        String[] allInfo = CONTROLLER.getCreatedRoom();
        String[][] parsedInfo = prepareForOut(allInfo);
        System.out.println("___________________");
        System.out.println("Name      | Points|");
        System.out.println("----------|-------|");
        for (int i = 0; i < CONTROLLER.getMaxDoors(); i++) {
            System.out.print(expandString(parsedInfo[i][0],
                    sizeForExpandingCreations)
                    + "|   " + expandString(parsedInfo[i][1],
                    sizeForExpandingPoints)
                    + "|" + " - door № " + (i + 1) + "\n");
        }
        System.out.println("-------------------");
        System.out.println("Fatal doors : " + CONTROLLER.getFatalDoors());
        System.out.println("The best order of doors for being alive : "
                + CONTROLLER.getOrderOfDoors());
    }

    private String[][] prepareForOut(final String[] allInfo) {
        String[][] parsedInfo = new String[CONTROLLER.getMaxDoors()][2];
        for (int i = 0; i < CONTROLLER.getMaxDoors(); i++) {
            parsedInfo[i] = allInfo[i].split("\\{");
            parsedInfo[i][1] = parsedInfo[i][1].split("=")[1].split("\\}")[0];
        }
        return parsedInfo;
    }

    private void showArrOperations() {
        System.out.println("All generated arrays : "
                + CONTROLLER.getAllArrays());
        System.out.println("General elements : "
                + CONTROLLER.getGeneralElements());
        System.out.println("Separate elements from first array: "
                + CONTROLLER.getSeparateElements());
        System.out.println("First array with deleted repeated elements, "
                + "that repeats two more times : "
                + CONTROLLER.deleteRepeatedTwoMore());
        System.out.println("Delete all elements except one that "
                + "are placed one by one : "
                + CONTROLLER.deleteSimilarExceptOne());
    }

    private String expandString(final String str, final int sizeForExpanding) {
        StringBuilder expanded = new StringBuilder(str);
        if (expanded.length() != sizeForExpanding) {
            for (int i = expanded.length(); i < sizeForExpanding; i++) {
                expanded.append(" ");
            }
        } else {
            String logMessage = "Can't expand this expanded : " + expanded;
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        return expanded.toString();
    }
}
